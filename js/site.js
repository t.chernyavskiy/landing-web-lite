$(window).on('load', function(){
    $('.preloader').delay(500).fadeOut('slow');
});
$(document).ready(function () {
    $('#find__on__map h2').on('click',function () {
       $('#find__on__map .map').fadeOut(200);
    });
    $('.the__team_carousel').owlCarousel({
        dots: true,
        smartSpeed: 600,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    $('.testimonials_carousel').owlCarousel({
        dots: true,
        smartSpeed: 600,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    $('.blog_carousel').owlCarousel({
        dots: true,
        smartSpeed: 600,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    var wookmark1 = new Wookmark('.portfolio__wookmark', {
        outerOffset: 5,
        itemWidth: 285,
        comparator: false
    });
});