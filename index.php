<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="stylesheet" href="css/media.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wookmark.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/site.js"></script>
    <title>Web-lite</title>
    <style>
        @-webkit-keyframes pulse-outer {
            0% {
                opacity: 1
            }

            50% {
                opacity: .5
            }

            100% {
                opacity: 0
            }
        }

        @keyframes pulse-outer {
            0% {
                opacity: 1
            }

            50% {
                opacity: .5
            }

            100% {
                opacity: 0
            }
        }

        @-webkit-keyframes pulse-inner {
            0% {
                opacity: 0;
                -webkit-transform: scale(0);
                transform: scale(0)
            }

            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes pulse-inner {
            0% {
                opacity: 0;
                -webkit-transform: scale(0);
                transform: scale(0)
            }

            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }
        .preloader .pulse::before {
            -webkit-animation: pulse-outer .8s ease-in infinite;
            animation: pulse-outer .8s ease-in infinite;
        }
        .preloader .pulse::after {
            -webkit-animation: pulse-inner .8s ease-in infinite;
            animation: pulse-inner .8s ease-in infinite;
        }
        .preloader .pulse::after, .preloader .pulse::before {
            content: '';
            border: 5px solid #19bd9a;
            width: 80px;
            height: 80px;
            -webkit-border-radius: 500px;
            border-radius: 500px;
            position: absolute;
        }
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 100;
            background-color: whitesmoke;
        }
        .preloader .pulse {
            position: relative;
            left: 50%;
            top: 50vh;
            margin-left: -40px;
            margin-top: -40px;
        }
    </style>
</head>
<body>
    <div class="preloader">
        <div class="pulse"></div>
    </div>
    <section id="header">
        <div class="container">
            <div class="row">
                <div class="header__menu">
                    <nav class="navbar navbar-default main__menu">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand logo__text" href="#">Company</a>
                            </div>

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Hello</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">Portfolio</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="header__body">
                    <h2>We Are Code Cafe</h2>
                    <p>
                        At vero eos et accusamus et iusto odio dignissimos
                        ducimus qui blanditiis praesentium voluptatum
                    </p>
                </div>
                <i class="fa fa-chevron-down go__next" aria-hidden="true"></i>
            </div>
        </div>
    </section>
    <section id="this__is">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-8 this__is_img">
                    <img src="images/IPAD.png" alt="ipad" class="this__is_ipad">
                </div>
                <div class="col-md-6 col-md-pull-4">
                    <h2>This is Bouncy</h2>
                    <p>
                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                        cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est
                        laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                    </p>
                    <a href="" class="this__is_more">Read More</a>
                </div>
            </div>
        </div>
    </section>
    <section id="details">
        <div class="container">
            <div class="row">
                <div class="details__title">
                    <h2>DetailS ABOUT BOUNCY</h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                    <i class="fa fa-heart-o details__icon"></i>
                    <i class="fa fa-lightbulb-o details__icon"></i>
                    <i class="fa fa-bell-o details__icon"></i>
                </div>
                <div class="details__idea">
                    <p>Creative Ideas</p>
                    <span>
                        At vero eos et accusamus et iusto odio dignissimos ducimus qui
                        blanditiis praesentium voluptatum deleniti atque corrupti quos dolores
                    </span>
                    <span>
                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati
                        cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi,
                        id est laborum et dolorum fuga. Et harum quidem r
                    </span>
                </div>
            </div>
        </div>
    </section>
    <section id="featured__projects">
        <div class="container">
            <div class="row">
                <h2>FEATURED PROJECTS</h2>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
            </div>
        </div>
        <div class="featured__work work1 featured__work_active"></div>
        <div class="featured__work work2"><span>vintage</span></div>
        <div class="featured__work work3"><span>branding</span></div>
        <div class="featured__work_footer">
            <span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis </span>
            <a href="#">Submit Now</a>
        </div>
    </section>
    <section id="our__services">
        <div class="container">
            <div class="row">
                <h2>our services</h2>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                <div class="services__icons col-md-1">
                    <i class="fa fa-wrench services__icon"></i>
                    <i class="fa fa-paint-brush services__icon"></i>
                    <i class="fa fa-gamepad services__icon"></i>
                    <i class="fa fa-plug services__icon"></i>
                </div>
                <div class="services__text col-md-5">
                    <p>Graphic Design</p>
                    <span>
                        At vero eos et accusamus et iusto odio dignissimos ducimus
                        qui blanditiis praesentium voluptatum deleniti
                    </span>
                    <br>
                    <span>
                        quos dolores et quas molestias excepturi sint occaecati cupiditate non provident,
                        similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
                    </span>
                </div>
                <div class="services__indicators col-md-6">
                    <div class="indicator">
                        <img src="images/Ellipse-branding.png" alt="indicator branding">
                        <p>Branding</p>
                    </div>
                    <div class="indicator">
                        <img src="images/Ellipse-design.png" alt="indicator web design">
                        <p>Web Design</p>
                    </div>
                    <div class="indicator">
                        <img src="images/Ellipse-UX.png" alt="indicator ui/ux">
                        <p>UI/UX</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team__quotes">
        <div class="container">
            <div class="row">
                <div class="quote__img col-md-5 col-md-push-7">
                    <img src="images/Bouncy-monitor.png" class="quote__monitor"
                         alt="bouncy monitor"/>
                </div>
                <div class="quote__text col-md-7 col-md-pull-5">
                    <h2>Impressed ?</h2>
                    <span>At vero eos et accusamus et iusto odio dignissimos ducimus qui
                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                        dolores et quas molestias
                    </span>
                    <p>- Abdullah Noman <br>
                        Creatice Director, Code Cafe</p>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio" >
    <div class="container">
        <div class="row">
            <h2>THE PORTFOLIO</h2>
            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
            <nav class="navbar navbar-default porfolio__menu">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <ul class="nav navbar-nav">
                            <li><a href="#" class="porfolio__menu_active">All Works</a></li>
                            <li><a href="#">Print</a></li>
                            <li><a href="#">Identity</a></li>
                            <li><a href="#">Branding</a></li>
                            <li><a href="#">Web</a></li>
                            <li><a href="#">HTML</a></li>
                            <li><a href="#">Wordpress</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <ul class="portfolio__wookmark">
                <li><img src="images/portfolio12.png" alt="portfolio12"></li>
                <li><img src="images/portfolio11.png" alt="portfolio11"></li>
                <li><img src="images/portfolio10.png" alt="portfolio10"></li>
                <li><img src="images/portfolio9.png" alt="portfolio9"></li>
                <li><img src="images/portfolio8.png" alt="portfolio8"></li>
                <li><img src="images/portfolio7.png" alt="portfolio7"></li>
                <li><img src="images/portfolio6.png" alt="portfolio6"></li>
                <li><img src="images/portfolio5.png" alt="portfolio5"></li>
                <li><img src="images/portfolio4.png" alt="portfolio4"></li>
                <li><img src="images/portfolio3.png" alt="portfolio3"></li>
                <li><img src="images/portfolio2.png" alt="portfolio2"></li>
                <li><img src="images/portfolio1.png" alt="portfolio1"></li>
            </ul>
            <div class="section__footer portfolio__footer">
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis </p>
                <a href="#">Start Project</a>
            </div>
        </div>
    </div>
</section>
    <section id="the__team">
    <div class="container">
        <div class="row">
            <h2>THE TEAM</h2>
            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
            <div class="owl-carousel owl-theme the__team_carousel">
                <div class="item">
                    <div class="teammate__img col-md-7">
                        <img src="images/teammate.png" alt="teammate">
                    </div>
                    <div class="teammate__info col-md-5">
                        <div class="teammate__name">
                            <p>Abdullah Noman</p>
                            <span class="teammate__post">Creative Director, Code Cafe</span>
                        </div>
                        <p class="teammate__description">
                            At vero eos et accusamus et iusto odio dignissimos
                            ducimus qui blanditiis praesentium
                        </p>
                        <div class="teammate__skill">
                            <div class="skill">
                                <p>branding</p>
                                <span>80%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>Web Design</p>
                                <span>65%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
                                        <span class="sr-only">65% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>User Interface</p>
                                <span>75%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                        <span class="sr-only">75% Complete</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="teammate__img col-md-7">
                        <img src="images/teammate.png" alt="teammate">
                    </div>
                    <div class="teammate__info col-md-5">
                        <div class="teammate__name">
                            <p>Pavel Filimonov</p>
                            <span class="teammate__post">Web developer, Code Cafe</span>
                        </div>
                        <p class="teammate__description">
                            At vero eos et accusamus et iusto odio dignissimos
                            ducimus qui blanditiis praesentium
                        </p>
                        <div class="teammate__skill">
                            <div class="skill">
                                <p>branding</p>
                                <span>70%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                        <span class="sr-only">70% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>Web Design</p>
                                <span>80%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>User Interface</p>
                                <span>79%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 79%;">
                                        <span class="sr-only">79% Complete</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="teammate__img col-md-7">
                        <img src="images/teammate.png" alt="teammate">
                    </div>
                    <div class="teammate__info col-md-5">
                        <div class="teammate__name">
                            <p>Abdullah Noman</p>
                            <span class="teammate__post">Creative Director, Code Cafe</span>
                        </div>
                        <p class="teammate__description">
                            At vero eos et accusamus et iusto odio dignissimos
                            ducimus qui blanditiis praesentium
                        </p>
                        <div class="teammate__skill">
                            <div class="skill">
                                <p>branding</p>
                                <span>80%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>Web Design</p>
                                <span>65%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
                                        <span class="sr-only">65% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="skill">
                                <p>User Interface</p>
                                <span>75%</span>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                        <span class="sr-only">75% Complete</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="section__footer the__team_footer">
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis </p>
                <a href="#">Start Project</a>
            </div>
        </div>
    </div>
</section>
    <section id="testimonials">
    <div class="container">
        <div class="row">
            <h2>Testimonials</h2>
            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
            <div class="testimonial col-md-offset-1 col-md-10">
                <div class="owl-carousel owl-theme testimonials_carousel">
                    <div class="item">
                        <div class="col-md-2">
                            <img src="images/testimonial.png" alt="testimonial">
                        </div>
                        <div class="col-md-10">
                            <span>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
                                occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt
                                mollitia.
                            </span>
                            <p><b>PARTHO</b></p>
                            <p>FOUNDER, ARTWAYS BD</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-2">
                            <img src="images/testimonial.png" alt="testimonial">
                        </div>
                        <div class="col-md-10">
                            <span>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen
                                book. It has survived not only five centuries, but also the leap into electronic
                                typesetting, remaining essentially unchanged.
                            </span>
                            <p><b>LIVERPOOL</b></p>
                            <p>FOUNDER, ARTWAYS BD</p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-2">
                            <img src="images/testimonial.png" alt="testimonial">
                        </div>
                        <div class="col-md-10">
                            <span>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
                                occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt
                                mollitia.
                            </span>
                            <p><b>PARTHO</b></p>
                            <p>FOUNDER, ARTWAYS BD</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme blog_carousel col-md-10">
                    <div class="item col-md-10">
                        <div class="news">
                            <h2>LATEST NEWS</h2>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                            <div class="article">
                                <h2>The trending Landing Pages</h2>
                                <p>by <span class="author">Kabir Uddin</span></p>
                                <p class="article__stats">6950 Likes - 243 Comments - 703 shares</p>
                                <p class="article__text">
                                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                    praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                                    excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui
                                    officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem
                                    rerum facilis est et expedita distinctio...
                                </p>
                                <p class="article__more">+ Read More</p>
                            </div>
                        </div>
                    </div>
                    <div class="item col-md-10">
                        <div class="videos">
                            <h2>LATEST Videos</h2>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                            <div class="video">
                                <iframe width="100%" height="415"
                                        src="https://www.youtube-nocookie.com/embed/D6tC1pyrsTM?showinfo=0"
                                        frameborder="0" allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="price">
        <div class="container">
            <div class="row">
                <h2>Our Price</h2>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                <div class="col-md-4 price__block">
                    <div class="tariff__block">
                        <p class="title">PREMIUM</p>
                        <p class="price"><span>$ 60</span> Per Month</p>
                        <ul class="info">
                            <li>Full Branding</li>
                            <li>Project Management</li>
                            <li>Unlimited Revisions</li>
                            <li>24/7 h Service</li>
                            <li>Free Domain</li>
                            <li>Free Hosting</li>
                        </ul>
                        <a href="#" class="tariff__start">Let’s Start ></a>
                    </div>
                </div>
                <div class="col-md-4 price__block">
                    <div class="tariff__block">
                        <p class="title">STANDART</p>
                        <p class="price"><span>$ 30</span> Per Month</p>
                        <ul class="info">
                            <li>Full Branding</li>
                            <li>Project Management</li>
                            <li>Unlimited Revisions</li>
                            <li>24/7 h Service</li>
                            <li>Free Domain</li>
                            <li>Free Hosting</li>
                        </ul>
                        <a href="#" class="tariff__start">Let’s Start ></a>
                    </div>
                </div>
                <div class="col-md-4 price__block">
                    <div class="tariff__block">
                        <p class="title">EXCLUSIVE</p>
                        <p class="price"><span>$ 80</span> Per Month</p>
                        <ul class="info">
                            <li>Full Branding</li>
                            <li>Project Management</li>
                            <li>Unlimited Revisions</li>
                            <li>24/7 h Service</li>
                            <li>Free Domain</li>
                            <li>Free Hosting</li>
                        </ul>
                        <a href="#" class="tariff__start">Let’s Start ></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="subscribe">
        <div class="container">
            <div class="row">
                <h2>Subscribe us</h2>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                <form action="#" method="post" class="col-md-12 subscribe__form">
                    <input type="email" placeholder="Email Address" class="col-md-offset-3 col-md-4 col-xs-7 email">
                    <input type="submit" value="SUBMIT" class="col-md-2 col-xs-4 submit">
                </form>
            </div>
        </div>
    </section>
    <section id="contacts">
        <div class="container">
            <div class="row">
                <h2>Contact Us</h2>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                <div class="col-md-4 contact__block">
                    <div class="contact__img">
                        <img src="images/contactUs-email.png" alt="email">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </div>
                    <div class="contact__title">
                        <p>EMAIL</p>
                        <span>
                            hello@codecafe.com <br>
                            Personal@codecafe.com
                        </span>
                    </div>
                </div>
                <div class="col-md-4 contact__block">
                    <div class="contact__img">
                        <img src="images/contactUs-office.png" alt="office">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                    </div>
                    <div class="contact__title">
                        <p>OUR OFFICE</p>
                        <span>240 Bhatikhana, 8200 Barisal Bangladesh</span>
                    </div>
                </div>
                <div class="col-md-4 contact__block">
                    <div class="contact__img">
                        <img src="images/contactUs-phone.png" alt="phone">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </div>
                    <div class="contact__title">
                        <p>PHONE</p>
                        <span>
                            +00 11 66 99 66 44 <br>
                            11 99 66 44 065
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contact__form">
        <div class="container">
            <div class="row">
                <form action="" method="post" class="contact__form">
                    <div class="col-md-4 form__input">
                        <input type="text" placeholder="Your name">
                    </div>
                    <div class="col-md-4 form__input">
                        <input type="text" placeholder="Email address">
                    </div>
                    <div class="col-md-4 form__input">
                        <input type="text" placeholder="Subject">
                    </div>
                    <div class="form__text">
                        <textarea name="text" id="" cols="30" rows="10" class="col-md-12"></textarea>
                    </div>
                    <div class="form__submit">
                        <input type="submit" class="col-md-12" value="SUBMIT NOW">
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section id="find__on__map">
        <div class="map">
            <h2>
                FIND US ON THE MAP <i class="fa fa-chevron-down find__us_arrow" aria-hidden="true"></i>
            </h2>
        </div>
    </section>
    <section id="footer">
        <div class="container">
            <div class="row">
                <h2>Bouncy</h2>
                <p>Copyright 2015. All rights Reserved</p>
                <div class="social__links">
                    <a href="https://www.facebook.com/" target="_blank"><img src="images/facebook-icon.png" alt="facebook"></a>
                    <a href="https://linkedin.com" target="_blank"><img src="images/linkedin-icon.png" alt="linkedin"></a>
                    <a href="https://twitter.com" target="_blank"><img src="images/twitter-icon.png" alt="twitter"></a>
                    <a href="https://vimeo.com" target="_blank"><img src="images/vimeo-icon.png" alt="vimeo"></a>
                    <a href="https://instagram.com" target="_blank"><img src="images/instagramm-icon.png" alt="instagramm"></a>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
<?php
